<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/15/2017
 * Time: 8:01 PM
 */

class Person{
    public $name;
    public function __construct()//call itself by php interpreter. Don't need to be call the method becace it's a magic methods;
    {
        echo "I'm inside the". __METHOD__ . "<br>";
    }
    public function __destruct()
    {
        echo"<br>";
        echo "I'm going! good bye! see you again!";// TODO: Implement __destruct() method.
    }

    public function __call($name, $arrayArguments)
    {
        echo "wrong method: $name". "<br>";// TODO: Implement __call() method.
        echo "<pre>";
            print_r($arrayArguments);
        echo "</pre>";
    }
    public static function __callStatic($name, $arrayArguments)
    {
        echo "wrong method: $name". "<br>";
        echo "<pre>";
        print_r($arrayArguments);
        echo "</pre>";// TODO: Implement __callStatic() method.
    }
    public function __set($name, $value)
    {
        echo "the wrong property is : $name"."<br>". "and the wrong value is $value"."<br>";// TODO: Implement __set() method.
    }
    public function __get($name)
    {
        echo "the wrong property is: $name";// TODO: Implement __get() method.
    }
    public function __isset($name)
    {
        echo "the inaccesable property is: $name";// TODO: Implement __isset() method.
    }
    public function __unset($name)
    {
        echo "inaccesable property: $name";// TODO: Implement __unset() method.
        echo "<br>";
    }


    public static function doSomething()
    {
        echo "I'm doing nothing!";
        echo "<br>";
    }

}

class Person2{
    public $name;
    public function __construct()//call itself by php interpreter. Don't need to be call the method becace it's a magic methods;
    {
        echo "I'm inside the". __METHOD__ . "<br>";
    }
}

Person::doSomething();
Person::doSomething1(1,2,3);

$object = new Person();
//unset($object);//by force destroying an object
$object2 = new Person();
$object->thisIsAWrongMethod(012,"rashu","wrong");
$object->wrongProperty="bullshit!";
echo $object->wrongPoperty;
serialize($object->nameNotFound);
unserialize($object->nameNotFound);

if(isset($object->wrongProperty))
{
    echo "the property is set!";
}
else
{
    echo "the property is not set!";
}
unset($object->wrongProperty);
